@extends('layouts.main')

@section('container')

    <div class="container mt-4">
        <div class="row row-cols-1 row-cols-md-2 g-4">
            <div class="col-md-6 text-center">
                <img src="img\1500Pro-EU-2.png" class="img-fluid" alt="Jackery 1000" style="max-height: 500px; margin: 0 auto;">
            </div>
            <div class="col-md-6">
                <h2><b>Jackery Explorer 1500 Pro Portable Power Station</b></h2>
                <h3 style="color: orangered"><b>Rp.23.999.000</b></h3>
                <div class="product-description">
                    <ul>
                    <li><span>1534Wh Capacity, 1800W AC Output </span><span>Output</span><span>&nbsp;</span></li>
                    <li><span>Power 7 Devices Simultaneously </span></li>
                    <li><span>Quick Recharging in 3 Simple Ways </span></li>
                    <li><span>Safe and Easy to Use </span></li>
                    <li><span>800 Cycles to 80%+ Capacity </span></li>
                    <li><span>Safe & Stable Lithium-ion Battery </span></li>
                    <li><span>5 Ways of Output for Various Usages</span></li>
                    </ul>
                </div>
                <a href="https://wa.link/9vriki"><button class="btn btn-danger">Beli Sekarang!</button></a>
            </div>
        </div>
    </div>

@endsection


