<nav class="navbar navbar-expand-lg navbar-dark bg-dark" style="position: relative; z-index: 1;">
    <div class="container">
        <a class="navbar-brand" href="/"> <img src="img\logo-jackery.png" class="img-fluid" alt="Logo" style="width: 120px"> </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExample07" aria-controls="navbarsExample07" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExample07">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link {{ ($title === "Home") ? 'active' : '' }}" href="/">Home</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="/product" role="button" data-bs-toggle="dropdown" aria-expanded="false">Product</a>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item {{ ($title === "Jackery Explorer 1000 Pro Portable Power Station") ? 'active' : '' }}" href="/product-jackery-1000">Jackery 1000</a></li>
                        <li><a class="dropdown-item {{ ($title === "Jackery Explorer 1500 Pro Portable Power Station") ? 'active' : '' }}" href="/product-jackery-1500">Jackery 1500</a></li>
                        <li><a class="dropdown-item {{ ($title === "Jackery Explorer 2000 Pro Portable Power Station") ? 'active' : '' }}" href="/product-jackery-2000">Jackery 2000</a></li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ ($title === "About") ? 'active' : '' }}" href="/about">About Us</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" role="button" data-bs-toggle="dropdown" aria-expanded="false">Contact Sales</a>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="https://mail.google.com/mail/?view=cm&fs=1&to=someone@example.com&su=SUBJECT&body=BODY&bcc=someone.else@example.com">Email</a></li>
                        <li><a class="dropdown-item" href="https://wa.link/9vriki">Whatsapp</a></li>
                        <li><a class="dropdown-item" href="https://shopee.co.id">Shopee</a></li>
                        <li><a class="dropdown-item" href="https://www.tokopedia.com/">Tokopedia</a></li>
                    </ul>
                </li>
            </ul>
            <form role="search">
                <input class="form-control" type="search" placeholder="Search" aria-label="Search">
            </form>
        </div>
    </div>
</nav>