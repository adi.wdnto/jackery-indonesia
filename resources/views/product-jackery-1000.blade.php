@extends('layouts.main')

@section('container')

    <div class="container mt-4">
        <div class="row row-cols-1 row-cols-md-2 g-4">
            <div class="col-md-6 text-center">
                <img src="img\E1000Pro-HTE081-JAK-EU-V1-3-0827.png" class="img-fluid" alt="Jackery 1000" style="max-height: 500px; margin: 0 auto;">
            </div>
            <div class="col-md-6">
                <h2><b>Jackery Explorer 1000 Pro Portable Power Station</b></h2>
                <h3 style="color: orangered"><b>Rp.15.999.000</b></h3>
                <div class="product-description">
                    <ul>
                    <li><span>1002Wh Capacity, 1000W (2000W Surge Power) </span><span>Output</span><span>&nbsp;</span></li>
                    <li><span>Supports 8 Devices Simultaneously </span></li>
                    <li><span>3 Ways to Recharge </span></li>
                    <li><span>Safe and Easy to Use </span></li>
                    <li><span>Long Battery Standby </span></li>
                    <li><span>Shock and Fire Resistance </span></li>
                    <li><span>Compact and Portable</span></li>
                    </ul>
                </div>
                <a href="https://wa.link/9vriki"><button class="btn btn-danger">Beli Sekarang!</button></a>
            </div>
        </div>
    </div>

@endsection


