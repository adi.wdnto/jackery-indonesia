@extends('layouts.main')

@section('container')

    <section class="wrapper">
		<div class="video-wrap">
			<video src="video\example.mp4" playsinline autoplay muted loop></video>
		</div>
		<div class="overlay"></div>

		<div class="container mt-4 landing-text">
			<h1>Jackery Indonesia</h1>
			<h3>Menciptakan masa depan berkelanjutan</h3>
			<a href="/about"><button class="btn btn-danger">Lebih lanjut</button> </a>
		</div>
	</section>

    <div class="container mt-4">
        <div class="text-center">
            <img src="img\sales-and-followers-jackery.png" class="img-fluid">
        </div>
    </div>

    <div class="container mt-4">
        <div class="container text-center mb-4">
            <h1 class="mb-4"><b>Pembelian Produk</b></h1>
            <div class="row row-cols-1 row-cols-md-4 g-4 justify-content-center">
                <div class="col d-flex justify-content-center">
                    <div class="image-container" style="width: 100px;"> <!-- Adjust width here -->
                        <a href="https://mail.google.com/mail/?view=cm&fs=1&to=someone@example.com&su=SUBJECT&body=BODY&bcc=someone.else@example.com"> <img src="img\email-logo-png-1121.png" href="https://wa.link/9vriki" class="img-fluid" alt="Email Logo"> </a>
                    </div>
                </div>
                <div class="col d-flex justify-content-center">
                    <div class="image-container" style="width: 100px;"> <!-- Adjust width here -->
                        <a href="https://wa.link/9vriki"> <img src="img\whatsapp-logo-png-2268.png" href="https://wa.link/9vriki" class="img-fluid" alt="Email Logo"> </a>
                    </div>
                </div>
                <div class="col d-flex justify-content-center">
                    <div class="image-container" style="width: 100px;"> <!-- Adjust width here -->
                        <a href="https://shopee.co.id"> <img src="img\shopee-logo-40482.png" href="https://wa.link/9vriki" class="img-fluid" alt="Email Logo"> </a>
                    </div>
                </div>
                <div class="col d-flex justify-content-center">
                    <div class="image-container" style="width: 100px;"> <!-- Adjust width here -->
                        <a href="https://www.tokopedia.com/"> <img src="img\tokopedia-38854.png" href="https://wa.link/9vriki" class="img-fluid" alt="Email Logo"> </a>
                    </div>
                </div>
            </div>
        </div>
    </div>    

    <div class="container mt-4">
        <div class="row row-cols-1 row-cols-md-2 g-4">
            <div class="col-md-6">
                <h2><b>Our Story</b></h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae justo non arcu condimentum facilisis at eu lorem. Nullam id mauris orci. Duis dapibus luctus dui, eget aliquet mi accumsan nec. Vestibulum tempor justo vel leo ullamcorper, vitae dictum tortor feugiat.</p>
                <p>Sed quis tincidunt odio. In hac habitasse platea dictumst. Vestibulum pharetra condimentum elit, vel egestas ipsum malesuada vel. Vivamus hendrerit ipsum sit amet nunc aliquam, ut facilisis elit tempus. Sed vehicula risus id elit egestas, ut tincidunt quam congue.</p>
            </div>
            <div class="col-md-6 d-flex justify-content-center align-items-center">
                <img src="img\E1000Pro-HTE081-EU-3.png" class="img-fluid" alt="Our Story Image">
            </div>
        </div>
    </div>

    <div class="container mt-4">
        <div class="row row-cols-1 row-cols-md-2 g-4">
            <div class="col-md-6 text-center">
                <img src="img\logo-quoted-jackery.png" class="img-fluid" alt="Our License Image" style="max-height: 300px; margin: 0 auto;">
            </div>
            <div class="col-md-6">
                <h2><b>Our License</b></h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae justo non arcu condimentum facilisis at eu lorem. Nullam id mauris orci. Duis dapibus luctus dui, eget aliquet mi accumsan nec. Vestibulum tempor justo vel leo ullamcorper, vitae dictum tortor feugiat.</p>
                <p>Sed quis tincidunt odio. In hac habitasse platea dictumst. Vestibulum pharetra condimentum elit, vel egestas ipsum malesuada vel. Vivamus hendrerit ipsum sit amet nunc aliquam, ut facilisis elit tempus. Sed vehicula risus id elit egestas, ut tincidunt quam congue.</p>
            </div>
        </div>
    </div>

    <div class="container mt-4">
        <div class="row row-cols-1 row-cols-md-2 g-4">
            <div class="col-md-6">
                <h2><b>How to Use</b></h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae justo non arcu condimentum facilisis at eu lorem. Nullam id mauris orci. Duis dapibus luctus dui, eget aliquet mi accumsan nec. Vestibulum tempor justo vel leo ullamcorper, vitae dictum tortor feugiat.</p>
                <p>Sed quis tincidunt odio. In hac habitasse platea dictumst. Vestibulum pharetra condimentum elit, vel egestas ipsum malesuada vel. Vivamus hendrerit ipsum sit amet nunc aliquam, ut facilisis elit tempus. Sed vehicula risus id elit egestas, ut tincidunt quam congue.</p>
                <button class="btn btn-danger">Read More</button>
            </div>
            <div class="col-md-6 d-flex justify-content-center align-items-center">
                <img src="img\E1000Pro-HTE081-EU-part.png" class="img-fluid" alt="How to Use Image">
            </div>
        </div>
    </div>

    <div class="container mt-4 text-center">
        <h1><b>Award and Recognition</b></h1>
        <h4>Jackery's efforts on R&D and sustainability are globally recognized.</h4>
        <div class="row row-cols-1 row-cols-md-4 g-4 justify-content-center">
            <div class="col-md-3">
                <img src="img/award-1.png" class="img-fluid mb-3" alt="Award 2">
            </div>
            <div class="col-md-3">
                <img src="img/award-2.png" class="img-fluid mb-3" alt="Award 2">
            </div>
            <div class="col-md-3">
                <img src="img/award-3.png" class="img-fluid mb-3" alt="Award 3">
            </div>
            <div class="col-md-3">
                <img src="img/award-4.png" class="img-fluid mb-3" alt="Award 4">
            </div>
        </div>
    </div>

@endsection


