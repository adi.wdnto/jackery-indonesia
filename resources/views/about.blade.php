@extends('layouts.main')

@section('container')

    <div class="container mt-4">
        <div class="row row-cols-1 row-cols-md-2 g-4">
            <div class="col-md-6 text-center">
                <h1>Tentang kami</h1>
            </div>
            <div class="col-md-6">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae justo non arcu condimentum facilisis at eu lorem. Nullam id mauris orci. Duis dapibus luctus dui, eget aliquet mi accumsan nec. Vestibulum tempor justo vel leo ullamcorper, vitae dictum tortor feugiat.</p>
                <p>Sed quis tincidunt odio. In hac habitasse platea dictumst. Vestibulum pharetra condimentum elit, vel egestas ipsum malesuada vel. Vivamus hendrerit ipsum sit amet nunc aliquam, ut facilisis elit tempus. Sed vehicula risus id elit egestas, ut tincidunt quam congue.</p>
            </div>
        </div>
    </div>

@endsection


