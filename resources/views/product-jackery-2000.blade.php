@extends('layouts.main')

@section('container')

    <div class="container mt-4">
        <div class="row row-cols-1 row-cols-md-2 g-4">
            <div class="col-md-6 text-center">
                <img src="img\E2000Pro-HTE078-EU-V2-3-0826.png" class="img-fluid" alt="Jackery 1000" style="max-height: 500px; margin: 0 auto;">
            </div>
            <div class="col-md-6">
                <h2><b>Jackery Explorer 2000 Pro Portable Power Station</b></h2>
                <h3 style="color: orangered"><b>Rp.30.399.000</b></h3>
                <div class="product-description">
                    <ul>
                    <li><span>Quick AC Charging in 2 Hours </span><span>Output</span><span>&nbsp;</span></li>
                    <li><span>2160Wh Capacity, 2200 W (4400W Peak) Output </span></li>
                    <li><span>Use Weekly for 10+ Years </span></li>
                    <li><span>Compact & Lightweight </span></li>
                    <li><span>3 Ways to Recharge </span></li>
                    </ul>
                </div>
                <a href="https://wa.link/9vriki"><button class="btn btn-danger">Beli Sekarang!</button></a>
            </div>
        </div>
    </div>

@endsection


