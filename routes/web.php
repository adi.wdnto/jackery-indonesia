<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home', [
        "title" => "Home"
    ]);
});

Route::get('/about', function () {
    return view('about', [
        "title" => "About"
    ]);
});

Route::get('/product-jackery-1000', function () {
    return view('product-jackery-1000', [
        "title" => "Jackery Explorer 1000 Pro Portable Power Station"
    ]);
});

Route::get('/product-jackery-1500', function () {
    return view('product-jackery-1500', [
        "title" => "Jackery Explorer 1500 Pro Portable Power Station"
    ]);
});

Route::get('/product-jackery-2000', function () {
    return view('product-jackery-2000', [
        "title" => "Jackery Explorer 2000 Pro Portable Power Station"
    ]);
});
